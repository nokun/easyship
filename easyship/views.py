from django.http import HttpResponse
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from .models import Shipment
#from django.db.models import Q
import json


def logoutView(request):
	#accounts = Account.objects.filter(owner=request.user)
	return redirect('/')

@login_required
def addView(request):
	if request.POST.get('id') == '0':
		Shipment.objects.create(owner=request.user, name=request.POST.get('name'), address=request.POST.get('address'))
	else:
		obj = Shipment.objects.get(owner=request.user, id=request.POST.get('id'))
		obj.name = request.POST.get('name')
		obj.address = request.POST.get('address')
		obj.save()
	
	return redirect('/')

@login_required
def homeView(request):
	query = ''
	if request.GET.get('q'):
		query = request.GET.get('q')

	shipments = Shipment.objects.raw('SELECT * FROM easyship_shipment WHERE owner_id = '+str(request.user.id)+' AND name LIKE \'%'+query+'%\'')
	return render(request, 'easyship/index.html',{'shipments':shipments,'query':query})
	
@login_required
def editView(request,id=-1):
	if id != -1:
		obj = Shipment.objects.get(id=id)
		return render(request, 'easyship/edit.html', {'id':id, 'name': obj.name,'address':obj.address, 'tracking':'ES'+str(id)+'FI'})
	else:
		return render(request, 'easyship/edit.html', {'id':0,'name': '', 'address':'','tracking':'To be generated'})

@login_required
def deleteView(request,id):
	obj = Shipment.objects.get(owner=request.user, id=id)
	obj.delete()
	return redirect('/')
	
	
