import hashlib 
import django.contrib.sessions.backends.db as db

# A safe (sic) session id generator
# because who will ever find out that the hash is so simple anyway?

class SessionStore(db.SessionStore):
	request = None
	
	def __init__(self, session_key=None, request=None):
		super(SessionStore, self).__init__(session_key)

	def _get_new_session_key(self):
		user = 'unknown'
		if self.request and self.request.POST.get("username"):
			user = self.request.POST.get("username")
		c = 0
		while True:
			c = c + 1 # a user may log in more than once
			session_key = hashlib.md5(('session number #'+str(c)).encode('utf-8')).hexdigest()
			if not self.exists(session_key):
				return session_key
				
